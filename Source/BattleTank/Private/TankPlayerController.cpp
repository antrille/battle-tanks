﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "TankPlayerController.h"
#include "Runtime/Engine/Classes/Engine/GameViewportClient.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"
#include "Runtime/UMG/Public/Blueprint/WidgetTree.h"
#include "Runtime/UMG/Public/Blueprint/WidgetLayoutLibrary.h"
#include "Runtime/UMG/Public/Components/CanvasPanelSlot.h"
#include "Tank.h"

void ATankPlayerController::BeginPlay()
{
	Super::BeginPlay();
}

void ATankPlayerController::SetPlayerUiReference(UUserWidget * UserWidget)
{
	PlayerUiWidget = UserWidget;
}

ATank* ATankPlayerController::GetControlledTank() const
{
	// Cast function already has a nullptr check inside
	return Cast<ATank>(GetPawn());
}

void ATankPlayerController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	AimTowardsCrosshair();
}

void ATankPlayerController::AimTowardsCrosshair() const
{
	const auto Tank = GetControlledTank();
	if (! Tank)
	{
		return;
	}
	
	FVector HitLocation;
	if (GetSightRayHitLocation(HitLocation))
	{
		GetControlledTank()->AimAt(HitLocation);
	}
}

bool ATankPlayerController::GetSightRayHitLocation(FVector& HitLocation) const
{
	if (!IsValid(PlayerUiWidget))
	{
		return false;
	}

	FVector AimDirection;
	if (!GetAimPointWorldDirection(AimDirection))
	{
		return false;
	}

	auto CameraPosition = PlayerCameraManager->GetCameraLocation();
	auto LineTraceEnd = CameraPosition + (AimDirection * 1000000.f);

	FHitResult HitResult;
	if (!GetWorld()->LineTraceSingleByChannel(HitResult, CameraPosition, LineTraceEnd, ECC_Visibility))
	{
		HitLocation = FVector(.0f);
		return false;
	}

	HitLocation = HitResult.Location;

	return true;
}

bool ATankPlayerController::GetAimPointWorldDirection(FVector& AimDirection) const
{
	auto ViewportClient = GetWorld()->GetGameViewport();
	auto AimPointWidget = PlayerUiWidget->WidgetTree->FindWidget("AimPoint");

	if (!IsValid(AimPointWidget))
	{
		UE_LOG(LogTemp, Error, TEXT("AimPoint widget is not found"));
		return false;
	}

	// Получаем координаты прицела на экране
	auto RootGeometry = PlayerUiWidget->GetCachedGeometry();
	auto AimPointGeometry = AimPointWidget->GetCachedGeometry();

	auto ScreenPosition = RootGeometry.AbsoluteToLocal(AimPointGeometry.GetAbsolutePosition());

	// Добавляем половину размера прицела к результирующей позиции, чтобы она приходилась на центр виджета
	ScreenPosition += AimPointGeometry.GetLocalSize() * .5f;

	// Применяем к позиции DPI масштабирование UI
	auto WidgetLayoutLibrary = NewObject<UWidgetLayoutLibrary>(UWidgetLayoutLibrary::StaticClass());
	auto Scale = WidgetLayoutLibrary->GetViewportScale(ViewportClient);

	ScreenPosition *= Scale;

	FVector WorldPosition;
	FVector ProjectedVector;

	if (!DeprojectScreenPositionToWorld(ScreenPosition.X, ScreenPosition.Y, WorldPosition, AimDirection))
	{
		UE_LOG(LogTemp, Error, TEXT("Can't deproject aim direction"));
		return false;
	}

	return true;
}


